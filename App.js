
import React from 'react'
import HomeScreen from './app/components/HomeScreen'
import StatsScreen from './app/components/StatsScreen'
import { createStackNavigator } from 'react-navigation'

const RootStack = createStackNavigator({
    Home: {
      screen: HomeScreen,
      title: 'Home'
    },

    Stats: {
        screen: StatsScreen,
        title: 'Stats'
    },

    initialRouteName: 'Home'
})

export default class App extends React.Component {
    render() {
      return <RootStack />;
    }
}