# base image
FROM node:9.11.2

# set working directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY package.json /usr/src/app/package.json

# install and cache app dependencies
RUN npm install
# RUN npm install create-react-native-app

#RUN npm install react-native-vector-icons -g --save
#RUN npm install react-native-scripts -g --silent

# start app
CMD ["npm", "start"]