### Eight Sleep React Native challenge

First, run:

```
$ npm install
$ npm start
```
Then, with `npm start` executed, you'll see a QR-code. Scan it with your QR-scanner app (keeping in mind, you have Expo app installed). Have fun.


Or, if you'd like to containerize your environment, you can use docker:

```
$ docker-compose -f docker-compose.yaml up
```

In this case, QR-code won't work by default. You'll need to connect to your machine directly by entering this addres in your mobile browser

```
exp://IP_ADDRESS:19000
```

To quickly obtain the whole address, you can use (either en0, or en1, en2 and so on):

```
$ echo $(ipconfig getifaddr en0):19000
```