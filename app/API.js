
import Config from './Config'

export default class API {
  // accepts a user id
  // returns intervals, if any
  static fetch(userId, onSuccess, onError = null) {
    return fetch(Config.api.rootURL + userId + '.json')
      .then((response) => response.json())
      .then((responseJson) => {
        onSuccess(responseJson.intervals)
      })
      .catch((error) => {
        onError(error)
      });
  }
}