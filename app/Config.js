
export default class Config {
    static api = {
        rootURL: 'https://s3.amazonaws.com/eight-public/challenge/'
    }

    static initDate = '2017-03-07'
}