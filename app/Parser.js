
import Events from './components/calendar/Events'

export default class Parser {
    static parseIntervals(intervals) {
        let _e = Events.events
        let _s = Events.stages

        // required to fix user's local time zone
        let localTimeOffset = new Date().getTimezoneOffset() / 60

        let events = []

        intervals.forEach((interval, i) => {
            var ts = new Date(interval.ts)
            ts.setHours(ts.getHours() + localTimeOffset)

            // stages
            let stages = interval.stages
            stages.forEach((stage, i) => {
                let duration = stage.duration
                
                start = new Date(ts)

                ts = new Date(ts.getTime() + 1000 * duration)

                end = new Date(ts)

                let event = {type: _e.stage, stage: stage.stage, start: start, end: end}

                events.push(event)
            })

            let timeSeries = interval.timeseries
            let tnts = timeSeries.tnt
            
            // tnt
            tnts.forEach((tnt, i) => {
                let date = new Date(tnt[0])

                // required to fix user's local time zone
                date.setHours(date.getHours() + localTimeOffset)

                let count = tnt[1]

                let event = {type: _e.tnt, tnts: count, start: date}

                events.push(event)
            })

            // stats
            let statNames = ['tempRoomC', 'tempBedC', 'respiratoryRate', 'heartRate']

            statNames.forEach((statName, i) => {
                let statData = timeSeries[statName]
                
                if(statData != null) {
                    statData.forEach((stat, i) => {
                    let date = new Date(stat[0])

                    // required to fix user's local time zone
                    date.setHours(date.getHours() + localTimeOffset)
                    
                    let endDate = new Date(date)
                    endDate.setHours(endDate.getHours() + 1)

                    let val = stat[1]

                    switch(statName) {
                        case 'tempRoomC':
                            val = '🏠 ' + val.toFixed(0) + ' ℃'
                        break
                        case 'tempBedC':
                            val = '🛏️ ' + val.toFixed(0) + ' ℃'
                        break
                        case 'respiratoryRate':
                            val = '\' ' + val.toFixed(0)
                        break
                        case 'heartRate':
                            val = '❤️ ' + val.toFixed(0)
                        break
                    }

                    let event = {type: _e.stat, start: date, end: endDate, title: val}

                    events.push(event)
                    //
                })
                }
            })
        })
    
        return events
    }
}