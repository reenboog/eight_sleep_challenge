import React from 'react'

import {FlatList, StyleSheet, View} from 'react-native'
import {ListItem } from 'react-native-elements'

import Users from '../Users.json'

class HomeScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Home'
    }
  }

  state = {
      users: null
  }

  componentDidMount() {
      this.loadUsers()
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 0.5,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    )
  }

  onUserSelected = (user) => {
    this.props.navigation.navigate('Stats', {
      user: user
    })
  }

  loadUsers = () => {
    this.setState({users: Users})
  }

  render() {
      const { users } = this.state
      
      return (
        <FlatList style = {style.scene}
            data = {users}
            renderItem = {({item}) => (
                <ListItem
                    roundAvatar
                    title = {item.name}
                    avatar = {{uri: item.avatar}}
                    containerStyle = {{ borderBottomWidth: 0 }}
                    onPress = {() => this.onUserSelected(item)}
                />
            )}
            keyExtractor = {i => i.id}
            ItemSeparatorComponent = {this.renderSeparator}
            onitem
        />
      )
  }
}

const style = StyleSheet.create({
    scene: {
        backgroundColor: '#fff',
        flex: 1
    }
})

export default HomeScreen