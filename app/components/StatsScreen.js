import React from 'react';
import { StyleSheet, ActivityIndicator, Text, View, Button, Image, Dimensions  } from 'react-native';

import API from '../API'
import Users from '../Users.json'
import Events from './calendar/Events'
import Parser from '../Parser'

import EventCalendar from './calendar/EventCalendar'
import Config from '../Config'

export default class StatsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    user = navigation.getParam('user', null)
    title = user.name
    
    return {
      title: title,
    };
  };

  constructor() {
    super()

    this.state = { 
      isLoading: true
    }
  }
  
  componentDidMount() {
    let user = this.props.navigation.getParam('user', null)
    let id = user.id

    API.fetch(id, (intervals) => {
      // parse, then set state
      let events = Parser.parseIntervals(intervals)

      this.setState({
        isLoading: false,
        events: events,
      }, () => {
        // setState callback
      });
    }, (error) => {
      console.log('Error: ' + error)
      
      this.setState({
        isLoading: false,
        isFailure: true
      })
    })
  }

  render () {
    if(this.state.isLoading) {
      return (
        <View style={{flex: 1,
          justifyContent: 'center',
          alignItems: 'center'}}>
          <ActivityIndicator/>
        </View>
      )
    } else if(this.state.isFailure) {
      return (
        <View style = {{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'}}>
          <Text style = {{
            color: 'rgb(170, 170, 170)',
            fontSize: 20}}>
            {'Failed to fetch user\'s stats'}
          </Text>
        </View>
      )
    } else {
      let { width } = Dimensions.get('window')

      let events = this.state.events

      return (
        <View style={{flex: 1}}>
          <EventCalendar
            
            events = {events}
            width = {width}
            numberOfDay = {60}
            initDate = {Config.initDate}
            scrollToFirst
          />
        </View>
      )
    }
  }
}
  