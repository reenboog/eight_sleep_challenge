
export default class Events {
    static events = {
        stat: 1,
        tnt: 2,
        stage: 3
    }
    
    static stages = {
        out: 'out',
        awake: 'awake',
        light: 'light',
        deep: 'deep'
    }
}