// @flow
import { StyleSheet } from 'react-native'
import Events from './Events'

const calendarHeight = 2400
// const eventPaddingLeft = 4
const leftMargin = 50 - 1
const rightMargin = 20

export default class Styles {
  static build (
    theme = {}
  ) {
    let style = {
      container: {
        flex: 1,
        backgroundColor: '#ffff',
        ...theme.container
      },
      contentStyle: {
        backgroundColor: '#ffff',
        height: calendarHeight + 10
      },
      header: {
        paddingHorizontal: 30,
        height: 50,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#E6E8F0',
        backgroundColor: '#F5F5F6',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        ...theme.header
      },
      headerText: {
        fontSize: 16
      },
      arrow: {
        width: 15,
        height: 15,
        opacity: 0.5,
        resizeMode: 'contain'
      },
      event: {
        position: 'absolute',
        backgroundColor: '#FFF',
        opacity: 0.8,
        borderColor: '#DDE5FD',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 4,
        paddingRight: 4,
        height: 30,
        alignSelf: 'flex-start',
        // minHeight: 25,
        // flex: 1,
        paddingTop: 5,
        // paddingBottom: 0,
        // flexDirection: 'column',
        // alignItems: 'flex-start',
        // overflow: 'hidden',
        ...theme.event
      },

      stage: {
        position: 'absolute',
        backgroundColor: 'rgba(20, 255, 20, 0.3)',
        width: 4,
        ...theme.stage
      },

      eventTitle: {
        color: '#615B73',
        fontWeight: '600',
        minHeight: 15,
        ...theme.eventTitle
      },
      line: {
        height: 1,
        position: 'absolute',
        left: leftMargin,
        backgroundColor: 'rgba(216, 216, 216, 0.2)',
        ...theme.line
      },
      lineNow: {
        height: 4,
        position: 'absolute',
        left: leftMargin,
        backgroundColor: 'rgba(216, 100, 100, 0.2)',
        ...theme.line
      },
      tnt : {
        position: 'absolute',
        right: rightMargin - 10,
        // paddingLeft: 4,
        // paddingRight: 4,
        alignSelf: 'flex-start',
        height: 30,
        // paddingTop: 5,
        ...theme.tnt
      },

      tntTitle: {
        color: '#615B73',
        fontWeight: '600',
        fontSize: 20,
        color: 'rgb(255, 20, 20)',
        ...theme.tntTitle
      },

      timeLabel: {
        position: 'absolute',
        left: 15,
        color: 'rgb(170,170,170)',
        fontSize: 10,
        // fontFamily: 'Helvetica Neue',
        fontWeight: '500',
        ...theme.timeLabel
      }
    }
    return StyleSheet.create(style)
  }

  static colorForStage(stage) {
    let stages = Events.stages

    switch(stage) {
      case stages.out:
        return 'rgba(255, 20, 20, 0.7)'
      case stages.awake:
        return 'rgba(20, 255, 20, 0.7)'
      case stages.light:
        return 'rgba(20, 20, 255, 0.7)'
      case stages.deep:
        return 'rgba(150, 150, 150, 0.7)'
    }
  }
}